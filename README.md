# News Web Application

These instructions will get you a copy of the project up and running on your local machine for evaluation purposes. 

This is a Single Page Application written in Angular 5, .Net Core 2.1 and using Entity Framework for local MSSQL database interaction. 

**Note:** The docker setup is likely not working and has not been tested.

## Install & Run

1. Once the repo is cloned, open a terminal window and navigate to folder ./NewsWebApp

2. Run command: 

	```
	dotnet run
	```

	This step may take a few minutes as all the angular dependencies are downloaded.

3. Navigate to http://localhost:49432 in you browser.

4. Login using one of the credentials below.

5. If you don't initially see any articles displayed, you can create new ones using the Manage Articles menu.

## Users

There are two users coded into the system. Each can be used for slightly different functionality:

* Publisher - Add/Update and publish articles.
* Employee - View articles.

#### Credentials - Publisher

**Username:** admin
**Password:** admin

#### Credentials - Employee

**Username:** employee
**Password:** employee

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsWebApp.NewsService;
using Xunit;
using Moq;
using NewsWebApp.Controllers;
using NewsWebApp.Models;

namespace NewsWebApp.Tests
{
    public class NewsWebAppTests
    {
        [Fact]
        public async Task GetAllArticlesTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();
            mockNewsRepository.Setup(nr => nr.GetAllArticles(It.IsAny<int>())).ReturnsAsync(GetNewsData);
           
            var controller = new NewsManagerController(mockNewsRepository.Object);
            var articles = await controller.GetAll();
 
            Assert.NotNull(articles);

        }

        [Fact]
        public async Task GetArticleTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();
            mockNewsRepository.Setup(nr => nr.Get(It.IsAny<int>())).ReturnsAsync(GetNewsData().FirstOrDefault);

            var controller = new NewsManagerController(mockNewsRepository.Object);
            var article = await controller.Get(It.IsAny<int>());

            Assert.NotNull(article);

        }

        [Fact]
        public async Task DeleteArticleTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();
            var controller = new NewsManagerController(mockNewsRepository.Object);

            await controller.Delete(It.IsAny<int>());
            mockNewsRepository.Verify(x=>x.Delete(It.IsAny<int>()), Times.Once);
        }


        [Fact]
        public async Task AddNewArticleTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();
          
            var controller = new NewsManagerController(mockNewsRepository.Object);

            await controller.Add(It.IsAny<Article>());
            mockNewsRepository.Verify(x=>x.Add(It.IsAny<Article>()), Times.Once);
        }

        [Fact]
        public async Task UpdateArticleTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();
          
            var controller = new NewsManagerController(mockNewsRepository.Object);
            var result = await controller.Update(It.IsAny<Article>());
            mockNewsRepository.Verify(x => x.Update(It.IsAny<Article>()), Times.Once);
        }


        [Fact]
        public async Task UpdateLikeCountTest()
        {
            var mockNewsRepository = new Mock<INewsRepository>();

            var controller = new NewsManagerController(mockNewsRepository.Object);
            await controller.UpdateLikeCount(It.IsAny<int>(), It.IsAny<string>());
            mockNewsRepository.Verify(x => x.UpdateLikeCount(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }




        public IEnumerable<Article> GetNewsData()
        {
            return new List<Article>
            {
                new Article
                {
                    Id = 1,
                    Author = "BBC",
                    Title = "Test",
                    Body = "Blahhhhh",
                    PublishDate = DateTime.Now
                },
                new Article
                {
                    Id = 2,
                    Author = "BBC",
                    Title = "Test",
                    Body = "Blahhhhh",
                    PublishDate = DateTime.Now
                }
            };
        }
    }
}
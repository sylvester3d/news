﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using NewsWebApp.Models;
using Newtonsoft.Json;
using Xunit;

namespace NewsWebApp.Tests
{
    public class NewsWebAppIntegrationTests
    {
        public NewsWebAppIntegrationTests()
        {
            _client = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()).CreateClient();
        }

        private readonly HttpClient _client;

        [Theory]
        [InlineData(1)]
        public async Task GetArticleByIdTest(int id)
        {
            var response = await _client.GetAsync("/api/newsmanager/"+id);
            var responseString = await response.Content.ReadAsStringAsync();
            var article = JsonConvert.DeserializeObject<Article>(responseString);

            Assert.NotNull(article);
            Assert.Equal(1, article.Id);
        }

        [Theory]
        [InlineData(1)]
        public async Task UpdateArticleTest(int id)
        {
            var response = await _client.GetAsync("/api/newsmanager/" + id);
            var responseString = await response.Content.ReadAsStringAsync();
            var article = JsonConvert.DeserializeObject<Article>(responseString);
            article.Title = "This is new title";

            var response2 = await _client.PutAsync("/api/newsmanager",
                new StringContent(JsonConvert.SerializeObject(article), Encoding.UTF8,
                    MediaTypeNames.Application.Json));

            Assert.Equal(HttpStatusCode.OK, response2.StatusCode);
        }

        private static Article GetArticleData()
        {
            var article = new Article
            {
                Title = "This is a title",
                Body = "This is a test",
                Author = "Admin",
                PublishDate = DateTime.Now,
                LikeCount = 0
            };
            return article;
        }

        [Fact]
        public async Task AddNewArticleTest()
        {
            var article = GetArticleData();
            var response = await _client.PostAsync("/api/newsmanager",
                new StringContent(JsonConvert.SerializeObject(article), Encoding.UTF8,
                    MediaTypeNames.Application.Json));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task DeleteArticleTest()
        {
            var article = GetArticleData();
            var responsePost = await _client.PostAsync("/api/newsmanager",
                new StringContent(JsonConvert.SerializeObject(article), Encoding.UTF8,
                    MediaTypeNames.Application.Json));

            var responseGet = await _client.GetAsync("/api/newsmanager");
            var responseStringGet = await responseGet.Content.ReadAsStringAsync();
            var articles = JsonConvert.DeserializeObject<IEnumerable<Article>>(responseStringGet).ToList();

            var responseDelete = await _client.DeleteAsync($"/api/newsmanager/" + articles.LastOrDefault()?.Id);

            Assert.Equal(HttpStatusCode.OK, responseDelete.StatusCode);
        }

        [Fact]
        public async Task GetAllArticlesTest()
        {
            var response = await _client.GetAsync("/api/newsmanager");
            var responseString = await response.Content.ReadAsStringAsync();
            var articles = JsonConvert.DeserializeObject<IEnumerable<Article>>(responseString).ToList();

            Assert.NotNull(articles);
            Assert.True(articles.Count > 0);
        }
    }
}
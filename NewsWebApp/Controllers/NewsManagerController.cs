﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsWebApp.Models;
using NewsWebApp.NewsService;

namespace NewsWebApp.Controllers
{
    [Route("api/newsmanager")]
    public class NewsManagerController : Controller
    {
       private readonly int _maxArticleBodyLength = 200;

       private readonly INewsRepository _newsRepository;
        
       public NewsManagerController(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Article>> GetAll()
        {
            return await _newsRepository.GetAllArticles(_maxArticleBodyLength);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var article = await _newsRepository.Get(id);
            return Ok(article);
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] Article article)
        {
            await _newsRepository.Add(article);
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody]Article article)
        {
            await _newsRepository.Update(article);
            return Ok();
        }

        [HttpPut]
        [Route("{id}/{username}")]
        public async Task<IActionResult> UpdateLikeCount(int id, string username)
        {
            await _newsRepository.UpdateLikeCount(id, username);
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _newsRepository.Delete(id);
            return Ok();
        }
        
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> AddComment([FromBody]Article article)
        {
            await _newsRepository.Update(article);
            return Ok();
        }
    }
}
﻿using Microsoft.EntityFrameworkCore;
using NewsWebApp.Models;

namespace NewsWebApp.NewsService
{
    public class NewsDbContext : DbContext
    {
        public NewsDbContext()
        {
        }

        public NewsDbContext(DbContextOptions<NewsDbContext> options)
            : base(options)
        {
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ArticleActivity> ActicleActivities { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(
                    "Server=(localdb)\\mssqllocaldb;Database=NewsDb;Trusted_Connection=True;ConnectRetryCount=0");
        }
    }
}
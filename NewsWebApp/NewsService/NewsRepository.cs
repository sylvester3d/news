﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NewsWebApp.Models;

namespace NewsWebApp.NewsService
{
    public class NewsRepository : INewsRepository
    {
        public readonly NewsDbContext Context = new NewsDbContext();

        public async Task<IEnumerable<Article>> GetAllArticles(int limitBodyLength)
        {
            return await Context.Articles.Select(
                a =>
                    new Article
                    {
                        Id = a.Id,
                        Title = a.Title,
                        Author = a.Author,
                        Comments = a.Comments,
                        LikeCount = a.LikeCount,
                        PublishDate = a.PublishDate,
                        Published = a.Published,
                        Body = limitBodyLength > 0 && a.Body != null
                            ? a.Body.Substring(0, a.Body.Length > limitBodyLength ? limitBodyLength : a.Body.Length)
                            : a.Body
                    }).ToListAsync();
        }

        public async Task Add(Article article)
        {
            article.PublishDate = DateTime.Now;
            Context.Articles.Add(article);
            await Context.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var article = Context.Articles.Find(id);
            Context.Articles.Remove(article);
            await Context.SaveChangesAsync();
        }

        public async Task<Article> Get(int id)
        {
            return await Context.Articles.FindAsync(id);
        }

        public async Task Update(Article article)
        {
            var original = Context.Articles.Find(article.Id);
            Context.Entry(original).CurrentValues.SetValues(article);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateLikeCount(int id, string username)
        {
            var existingRow = Context.ActicleActivities.Where(x => x.ArticleId == id && x.User == username);

            if (!existingRow.Any())
            {
                var articleActivity = new ArticleActivity { ArticleId = id, User = username };
                Context.ActicleActivities.Add(articleActivity);

                Article article = Context.Articles.SingleOrDefault(a => a.Id == id);

                if (article != null)
                {
                    article.LikeCount++;
                    Context.Articles.Update(article);
                }
            }

            await Context.SaveChangesAsync();
        }

    }
}
﻿using NewsWebApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsWebApp.NewsService
{
    public interface INewsRepository
    {
        Task<IEnumerable<Article>> GetAllArticles(int limitBodyLength);
        Task Add(Article article);
        Task Delete(int id);
        Task<Article> Get(int id);
        Task Update(Article article);
        Task UpdateLikeCount(int id, string username);
    }
}
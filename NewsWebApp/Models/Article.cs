﻿using System;
using System.Collections.Generic;

namespace NewsWebApp.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime PublishDate { get; set; }
        public string Author { get; set; }
        public int LikeCount { get; set; }
        public bool Published { get; set; }
        public IList<Comment> Comments { get; set; }
    }
}

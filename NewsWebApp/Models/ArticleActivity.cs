﻿namespace NewsWebApp.Models
{
    public class ArticleActivity
    {
        public int Id { get; set; }
        public int ArticleId { get; set; }
        public string User { get; set; }
    }
}

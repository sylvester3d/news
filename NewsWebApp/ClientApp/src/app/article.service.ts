import { Injectable, Inject } from '@angular/core'; 
import { Http, Response } from '@angular/http'; 
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router'; 
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch'; 
import 'rxjs/add/observable/throw'; 
import { Article } from './article';
 
@Injectable()
 
export class ArticleService {
 
  url: string = ""; 
  constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string) { 
      this.url = baseUrl; 
}

readArticles(): Observable<Article[]>{
      return this._http.get(this.url + 'api/newsmanager') 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler); 
}

getArticleById(id: number) { 
      return this._http.get(this.url + "api/newsmanager/" + id) 
          .map((response: Response) => response.json()) 
          .catch(this.errorHandler) 
  }

createArticle(article){
      return this._http.post(this.url + 'api/newsmanager', article) 
      .map((response: Response) => response.json()) 
      .catch(this.errorHandler); 
}

errorHandler(error: Response) { 
      console.log(error); 
      return Observable.throw(error); 
}
 
}
import { Component} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {private router: Router
  isExpanded =  false;
  userRole = '';

  checkLogin = function(){
    if (localStorage.getItem('userToken') != null && localStorage.getItem('role') != null) return true;
    return false;
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout() {
   
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  getUserRole() {
    this.userRole = localStorage.getItem('role');    
  }
    
  ngOnInit() {
    
  }
}

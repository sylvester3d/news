import { Component, OnInit, Inject} from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'; 
import { Article } from '../article';
import { Location } from '@angular/common';

@Component({
  selector: 'app-read-article',
  templateUrl: './read-article.component.html',
  styleUrls: ['./read-article.component.css']
})

export class ReadArticleComponent implements OnInit {

  constructor(private _http: Http, @Inject('BASE_URL') private _baseUrl: string, private _router: Router, private _route: ActivatedRoute,
  private location: Location) { }

  article:object = {};
  id: number;
  title = "Read Article";
  liked: boolean;

  getData = function(){
    this._route.params.subscribe(params => {
      this.id = params['id'];
  });

    this._http.get(this._baseUrl + 'api/newsmanager/'+ this.id).subscribe(result => {
      this.article = result.json();
      console.log(this.article);
    }, error => console.error(error));
  }

  likeArticle(){
    var username = localStorage.getItem('username');
    debugger
    this._http.put(this._baseUrl + 'api/newsmanager/'+ this.id +'/'+ username, null)
      .subscribe(
        result => { this.liked = true; }, 
        error => {
          console.error(error)
        });
  }

  addComment() {
    this._http.put(this._baseUrl + 'api/newsmanager', this.article).subscribe(result => {
    }, error => console.error(error));
  }

  goBack() {
    this.location.back(); 
  }

  ngOnInit() {
      this.getData();
      this.liked = false;
  }
}

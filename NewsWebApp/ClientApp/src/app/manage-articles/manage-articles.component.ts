import { Component, OnInit, Inject} from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'; 
import { Article } from '../article';

@Component({
  selector: 'app-manage-articles',
  templateUrl: './manage-articles.component.html',
  styleUrls: ['./manage-articles.component.css']
})
export class ManageArticlesComponent implements OnInit {
  id: number;
  
  constructor(private _http: Http, @Inject('BASE_URL') private _baseUrl: string, private _router: Router) {
  }
   
  title = "Manage Articles";
  articles = [];
  getData = function(){
    this._http.get(this._baseUrl + 'api/newsmanager').subscribe(result => {
      debugger
      this.articles = result.json();
    }, error => console.error(error));
  }

  deleteArticle = function(id){

      if(confirm("Do you want to delete this article?"))
      {
        this._http.delete(this._baseUrl + 'api/newsmanager/'+ id).subscribe(result => {
        console.log(result);
        this.getData();
        }, error => console.error(error));
      }
  
  }
  
  ngOnInit() {
    this.getData();
  }

}

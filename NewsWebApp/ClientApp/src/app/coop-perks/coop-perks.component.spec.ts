import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoopPerksComponent } from './coop-perks.component';

describe('CoopPerksComponent', () => {
  let component: CoopPerksComponent;
  let fixture: ComponentFixture<CoopPerksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoopPerksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoopPerksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class Article {
    constructor(
        public id: number,
        public title: string,
        public body: string,
        public author: string,
        public publishdate: Date,
        public published: boolean,
        public comments: string,
    ){}
}
import { Component, OnInit, Inject} from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'; 
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Location } from '@angular/common';


@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.css']
})
export class UpdateArticleComponent implements OnInit {

  update_article_form: FormGroup;
  
  constructor(private _http: Http, @Inject('BASE_URL') private _baseUrl: string, private _router: Router, private _route: ActivatedRoute,
  formBuilder: FormBuilder, private location: Location) {

    this.update_article_form = formBuilder.group({
      id: ["", Validators.required],
      title: ["", Validators.required],
      body: ["", Validators.required],
      author: ["", Validators.required],
      published: ["", Validators.required]
  });  
}

article:object = {};
id: number;
title = "Update Article";
confirmationMessage: string = "Article has been updated";
articleIsUpdated: boolean = false;

getData = function(){
  this._route.params.subscribe(params => {
    this.id = params['id'];
});

  this._http.get(this._baseUrl + 'api/newsmanager/'+ this.id).subscribe(result => {
    this.article = result.json();
  }, error => console.error(error));
}

updateArticle(){

  this._http.put(this._baseUrl + 'api/newsmanager', this.article).subscribe(result => {
    this.articleIsUpdated = true;
    this.getData();
  }, error => console.error(error));
}

goBack() {
  this.location.back(); 
}

ngOnInit() {
    this.getData();
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'; 
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Article } from '../article';
import { Location } from '@angular/common';
import {NgForm} from '@angular/forms';
import { UserService } from '../user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoginError : boolean = false;
  constructor(private userService : UserService, private router : Router, private http: HttpClient, @Inject('BASE_URL') private _baseUrl: string) { }

  ngOnInit() {
  }

  OnSubmit(username, password){

  this.userService.userAuthenticationLocal(username, password);
  var access_token = localStorage.getItem('userToken'); 

  if(access_token)
   {
      this.router.navigate(['/home']);
   }else
   {
      this.isLoginError = true;
   }
  };

}

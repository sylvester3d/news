import { Component, OnInit, Inject} from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router'; 
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Article } from '../article';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
 
    create_article_form: FormGroup;

    constructor(private _http: Http, @Inject('BASE_URL') private _baseUrl: string, private _router: Router,
        formBuilder: FormBuilder, private location: Location
    ){
        this.create_article_form = formBuilder.group({
            title: ["", Validators.required],
            body: ["", Validators.required],
            author: ["", Validators.required],
            published: ["", Validators.required]
        });
    }
    title = "Add New Article";
    confirmationMessage: string = "Article has been created";
    articleIsCreated: boolean = false;
 
    createArticle(){
      this._http.post(this._baseUrl + 'api/newsmanager', this.create_article_form.value).subscribe(result => {
          this.create_article_form.reset();
          this.articleIsCreated = true;
      }, error => console.error(error));
    }
 
    goBack() {
      this.location.back(); 
    }
    
    ngOnInit(){
           
    }
}

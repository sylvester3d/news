import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean {
      debugger
      if (localStorage.getItem('userToken') != null && localStorage.getItem('role') === 'Publisher')
      return true;
      this.router.navigate(['/login']);
      return false;
  }
}
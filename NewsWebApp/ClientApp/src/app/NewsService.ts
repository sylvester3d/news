import { Injectable, Inject } from '@angular/core'; 
import { Http, Response } from '@angular/http'; 
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router'; 
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch'; 
import 'rxjs/add/observable/throw'; 
   
@Injectable() 
export class NewsService { 
    url: string = ""; 
   
    constructor(private _http: Http, @Inject('BASE_URL') baseUrl: string) { 
        this.url = baseUrl; 
    } 
   
    getArticleList() { 
        return this._http.get(this.url + 'api/newsmanager') 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler); 
    } 
   
    getArticleById(id: number) { 
        return this._http.get(this.url + "api/newsmanager/" + id) 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler) 
    }

    saveArticle(article) { 
        return this._http.post(this.url + 'api/newsmanager', article) 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler); 
    } 
   
    updateArticle(article) { 
        return this._http.put(this.url + 'api/newsmanager/', article) 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler); 
    } 
   
    deleteArticle(id: number) { 
        return this._http.delete(this.url + "api/newsmanager/" + id) 
            .map((response: Response) => response.json()) 
            .catch(this.errorHandler); 
    } 

    errorHandler(error: Response) { 
        console.log(error); 
        return Observable.throw(error); 
    } 
}
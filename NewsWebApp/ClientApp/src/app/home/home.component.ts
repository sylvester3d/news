import { Component, OnInit, Inject} from '@angular/core';
import { Http, Response, Headers} from '@angular/http'
import { Router, ActivatedRoute } from '@angular/router'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  articles = [];
 

  constructor(private _http: Http, @Inject('BASE_URL') private _baseUrl: string, private _router: Router) {
      this._http.get(this._baseUrl + 'api/newsmanager').subscribe(result => {
        this.articles = result.json();
      }, error => console.error(error));
  }

  
  ngOnInit() {


  }

}


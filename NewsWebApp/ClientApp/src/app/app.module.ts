import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http'; 
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ManageArticlesComponent } from './manage-articles/manage-articles.component';
import { AddArticleComponent } from './add-article/add-article.component';
import { UpdateArticleComponent } from './update-article/update-article.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './user.service';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { ReadArticleComponent } from './read-article/read-article.component';
import { NoticeboardComponent } from './noticeboard/noticeboard.component';
import { LunchMenuComponent } from './lunch-menu/lunch-menu.component';
import { CoopPerksComponent } from './coop-perks/coop-perks.component';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ManageArticlesComponent,
    AddArticleComponent,
    UpdateArticleComponent,
    LoginComponent,
    ReadArticleComponent,
    NoticeboardComponent,
    LunchMenuComponent,
    CoopPerksComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule, 
    RouterModule.forRoot([

      { path: 'home', component: HomeComponent},       
      { path: 'manage-articles', component: ManageArticlesComponent, canActivate:[AuthGuard]},
      { path: 'profile', component: ProfileComponent},  
      { path: 'add-article', component: AddArticleComponent, canActivate:[AuthGuard]}, 
      { path: 'update-article/:id', component: UpdateArticleComponent, canActivate:[AuthGuard] },
      { path: 'read-article/:id', component: ReadArticleComponent},
      { path: 'lunch-menu', component: LunchMenuComponent },
      { path: 'coop-perks', component: CoopPerksComponent },
      { path: 'login', component: LoginComponent },   
      { path: 'noticeboard', component: NoticeboardComponent }, 
      { path : '', redirectTo:'/login', pathMatch : 'full'}

    ])
  ],
  
  providers: [UserService,AuthGuard,
    {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthInterceptor,
      multi : true
    }],

  bootstrap: [AppComponent]
})
export class AppModule { }
